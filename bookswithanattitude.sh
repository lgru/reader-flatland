#!/bin/bash

convert -background red -bordercolor red -size 700 -border 30 pango:'<span font="120" face="monospace" color="white">Books With An Attitude</span>' slide.jpg;
feh -x --fullscreen 'slide.jpg'

convert -background red -bordercolor red -size 700 -border 30 pango:'<span font="120" face="monospace" color="white">E-Books With An Attitude</span>' slide.jpg;
feh -x --fullscreen 'slide.jpg'

#Hommage to Ward Cunningham
 convert -background red -bordercolor red -size 700 -border 30 pango:'<span font="25" face="monospace" color="white">Ward Cunningham (1995): You are browsing a database with a program called Wiki Wiki Web. And <b>the program has an attitude</b>. The program wants everyone to be an author. So, the program slants in favor of authors at some inconvenience to readers.</span>' slide.jpg;
feh -x --fullscreen 'slide.jpg';

#Hommage to Paul Otlet
#feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/001-Otlet_Univers_Livre.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/002-multi_otlet.jpg'

#WHO AMI

feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/P1000836.JPG'

convert -background red -bordercolor red -size 700 -border 30 pango:'<span font="45" face="monospace" color="white">Agave, ConTeXt, FontForge, FontMatrix, gedit, GIMP, Imagemagick, Inkscape, Laidout, Libreoffice, pdf2text, pdftops, PoDoFo, poster, psnup, Python, Scribus, Sigil, TeX, ... </span>' slide.jpg;
feh -x --fullscreen 'slide.jpg';

#COMMUNITY

feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/IMG_0318.JPG'

#WHAT ARE BOOKS WITH AN ATTITUDE?

convert -background red -bordercolor red -size 700 -border 30 pango:'<span font="30" face="monospace" color="white">Cory Doctorow (2004): The  distinctive value of ebooks is orthagonal to the value of paper books, and it revolves around the <b>mix-ability and send-ability</b> of electronic text. The more you constrain an ebook‘s distinctive value propositions — that is, the more you restrict a reader‘s ability to copy, transport or transform an ebook — the more it has to be valued on the same axes as a paper-book.</span>' slide.jpg;
feh -x --fullscreen 'slide.jpg';

convert -background red -bordercolor red -size 700 -border 30 pango:'<span font="30" face="monospace" color="white"><b>Books with an attitude</b>
- Favour open content: re-usable, remixable
- Are made with Free Software tools
- Make their sources available
- Treat readers as writers and vice versa
</span>' slide.jpg; feh -x --fullscreen 'slide.jpg'

convert -background red -bordercolor red -size 700 -border 30 pango:'<span font="30" face="monospace" color="white"><b>Books with an attitude</b>
- Expand the scope of e-books
- Move from web to print and back
- Play with different devices as potential
- Do not separate content, form and technology
</span>' slide.jpg; feh -x --fullscreen 'slide.jpg'

#THE LIBRARY

#56 Broken Kindle Screens

convert -background lightgrey -bordercolor lightgrey -fill black -size 700 -border 20 pango:'<span font="30" face="monospace"><b><i>56 Broken Kindle Screens</i></b>

Design and development: Silvio Lorusso with Sebastian Schmieg 
Tools: ?
Year: 2012
</span>' slide.jpg; feh --fullscreen 'slide.jpg'

#feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/56bks_main_white.jpg'

feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/IMG_20431.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/IMG_20462.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/IMG_20531.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/IMG_20551.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/IMG_20571.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/IMG_20641.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/IMG_20751.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/IMG_20871.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/IMG_20981.jpg'

#mplayer images/56brokenkindlescreens.mp4

#Aether9

convert -background lightgrey -bordercolor lightgrey -fill black -size 700 -border 20 pango:'<span font="30" face="monospace"><b><i>Aether9</i></b>

Design: OSP (Gijs de Heij, Ludivine Loiseau, Pierre Marchand)
Tools: ConTeXt, Inkscape, OSPImpose
Year: 2012
</span>' slide.jpg; feh --fullscreen 'slide.jpg'

feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/book-01.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/book-02.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/book-03.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/book-04.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/book-05.jpg'

#Balsamine

convert -background lightgrey -bordercolor lightgrey -fill black -size 700 -border 20 pango:'<span font="30" face="monospace"><b><i>Balsamine</i></b>

Design and development: OSP (Sarah Magnan, Pierre Huyghebaert, Ludivine Loiseau)
Tools: Etherpad, text-editor, chrome browser
Year: 2013
</span>' slide.jpg; feh --fullscreen 'slide.jpg'

feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/IMG_20130514_192935.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/IMG_20130514_192943.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/screenshot_2013-04-30_01.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/screenshot_2013-04-30_02.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/screenshot_2013-04-30_03.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/screenshot_2013-04-30_13.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/screenshot_2013-04-30.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/screenshot_2013-05-0_16.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/screenshot_2013-05-02_19.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/screenshot_2013-05-02_20.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/screenshot_2013-05-04_15.png'

#La Carte Ou le Territoire

convert -background lightgrey -bordercolor lightgrey -fill black -size 700 -border 20 pango:'<span font="30" face="monospace"><b><i>La Carte Ou le Territoire</i></b>

Design and development: Stéphanie Vilayphiou
Tools: jQuery, python 
Year: 2012</span>' slide.jpg; feh --fullscreen 'slide.jpg'

feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/bcc-lacarteouleterritoire.gif'

firefox 'http://bcc.stdin.fr/LaCarteOuLeTerritoire' & disown; read

#Considering your tools: a reader for designers and developers

convert -background lightgrey -bordercolor lightgrey -fill black -size 700 -border 20 pango:'<span font="30" face="monospace"><b><i>Considering your tools: a reader for designers and developers</i></b>

Editing, design and development: Stéphanie Vilayphiou, Alex Leray
Tools: jQuery, python, custom wiki software 
Year: 2012
</span>' slide.jpg; feh --fullscreen 'slide.jpg'

firefox 'http://reader.lgru.net' & disown; read

#The Death of the Authors

convert -background lightgrey -bordercolor lightgrey -fill black -size 700 -border 20 pango:'<span font="30" face="monospace"><b><i>The Death of the Authors</i></b>

Design and development: Constant (An Mertens, Femke Snelting, Denis Devos)
Tools: ConText, Python, php
Year: 2012 
</span>' slide.jpg; feh --fullscreen 'slide.jpg'

firefox 'http://publicdomainday.constantvzw.org/1941.php' & disown; read
firefox 'http://publicdomainday.constantvzw.org/1941.php' & disown; read

#Exquisite Code

convert -background lightgrey -bordercolor lightgrey -fill black -size 700 -border 20 pango:'<span font="30" face="monospace"><b><i>Exquisite Code</i></b>

Design, development: Brendan Howell
Writing: Multiple authors
Tools: NLTK, Python
Year: 2008-2010
</span>' slide.jpg; feh --fullscreen 'slide.jpg'

feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/p1040699.jpg'

mplayer -ss 0:37 -endpos 0:51 images/excuisitecode.mp4

#Flossmanuals: Inkscape

convert -background lightgrey -bordercolor lightgrey -fill black -size 700 -border 20 pango:'<span font="30" face="monospace"><b><i>Flossmanuals: Inkscape</i></b>

Editing: Joshua Facemyer, adam hyde, Cedric Gemy, Elisa de Castro Guerra, Alexandre Prokoudine, John Curwood, Nevit Dilmen, Gondouin-Liu, Rafe DiDomenico, Austin Martin 
Tools: Inkscape, Booki
Year: 2008, 2010
</span>' slide.jpg; feh --fullscreen 'slide.jpg'

firefox 'inkscape.epub' & disown; read

#Funzie fonzie Leesmasjien

convert -background lightgrey -bordercolor lightgrey -fill black -size 700 -border 20 pango:'<span font="30" face="monospace"><b><i>Funzie fonzie Leesmasjien</i></b>

Hardware: Michael Korntheur
Software: Pierre Marchand
Tools: Fonzie, bookscanner
Year: 2012</span>' slide.jpg; feh --fullscreen 'slide.jpg'

feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/ScanbotA1.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/IMG_7115.JPG'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/scanbot_01.JPG'


evince 'images/v.pdf' & disown; read
evince 'images/fonzie-newsgothic.pdf' & disown; read

#Librivox: Stops, or How to Punctuate

convert -background lightgrey -bordercolor lightgrey -fill black -size 700 -border 20 pango:'<span font="30" face="monospace"><b><i>Stops, or How to Punctuate</i></b>

Text: Paul Allardyce
Reading: Zachary Brewster-Geisz, Laurie Anne Walden
Tools: Audacity
2008</span>' slide.jpg; feh --fullscreen 'slide.jpg'

firefox 'http://librivox.org' 'http://librivox.org/stops-or-how-to-punctuate-by-paul-allardyce' & disown; read
mplayer -endpos 0:51 stops_02_allardyce.mp3

#The SKOR Codex

convert -background lightgrey -bordercolor lightgrey -fill black -size 700 -border 20 pango:'<span font="30" face="monospace"><b><i>The SKOR Codex</i></b>

Design and development: La Société Anonyme 
Tools: ?
2012</span>' slide.jpg; feh --fullscreen 'slide.jpg'

#feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/codex-book_side.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/codex-books_opened_2.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/codex-books_opened.png'
#feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/codex-handing.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/codex-open-1.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/codex-open-2.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/codex-open-3.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/codex-open-4.png'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/codex-side_open.png'

#Univers Else

convert -background lightgrey -bordercolor lightgrey -fill black -size 700 -border 20 pango:'<span font="30" face="monospace"><b><i>Univers Else</i></b>

Design and development: OSP, Speculoos 
Tools: Fonzie, potrace, scanner
2009</span>' slide.jpg; feh --fullscreen 'slide.jpg'

feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/universelse_A.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/universelse_B.png'

#The Weise7 in/compatible Laboratorium Archive

convert -background lightgrey -bordercolor lightgrey -fill black -size 700 -border 20 pango:'<span font="30" face="monospace"><b><i>The Weise7 in/compatible Laboratorium Archive</i></b>

Editing: Kristoffer Gansing
Design and development : Bengt Sjolen
Tools: OpenWRT GNU/Linux, InDesign (?)
2013
</span>' slide.jpg; feh --fullscreen 'slide.jpg'

feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/weise7-book-home0.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/weise7-book-home1.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/weise7-book-home2.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/weise7-book-home3.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/weise7-book-home4.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/weise7-book-home5.jpg'

#CONCLUSION

convert -background red -bordercolor red -size 700 -border 30 pango:'<span font="120" face="monospace" color="white">Books With An Attitude</span>' slide.jpg;
feh -x --fullscreen 'slide.jpg'

feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images/002-multi_otlet.jpg'
feh -x --fullscreen --zoom max --draw-tinted -K captions/ 'images//responsive-wordpressdotcom-e1358480556937.jpg'

convert -background red -bordercolor red -size 700 -border 30 pango:'<span font="30" face="monospace" color="white">snelting@collectifs.net</span>' slide.jpg;
feh -x --fullscreen 'slide.jpg'

#######################################################################

#images/aether9_essay_by_Judy-Nylon.pdf
#images/aether9_interview_osp_EN.pdf
#images/book_web_sketch05.png
#images/e-book-layout.jpg
#images/maggot_sw_fin_1.jpg
#images/mkk-finland-web.jpg

#images/product-shot-01.jpg
#images/pzi_A.JPG
#images/pzi_B.png
#images/pzi_C.JPG

#images/tracks_C.png
#images/tracks_D.png
#images/tracks_E.png

#libreoffice --nologo --norestore bookswithanattitude.odt & disown; read;




