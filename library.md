![](images/responsive-wordpressdotcom-e1358480556937.jpg)

Library of Books with an attitude
========

What is this collection about. Not always books, also reading/writing systems.

An e-book with an attitude:

- Opens up its own internal processes, tools, sources
- Has a sense of context, meaning considers itself as part of a whole chain/ecology of practices
- Aims to put reading and writing at the same level, or at least experiments with non-conventional divisions of roles
- Plays with reading and writing of both text and code; treats technical content as part of a reading and writing experience
- Is networked and/or mutable
- Works with specific characteristics of specific devices/reading contexts, taking advantage of intrinsic qualities.

56 Broken Kindle Screens
--------

![](images/p1040699.jpg)

* Design and development: **Silvio Lorusso, Sebastian Schmieg**
* Tools: **?**
* Date: **2012**
* [http://sebastianschmieg.com/56brokenkindlescreens/](http://sebastianschmieg.com/56brokenkindlescreens/)

_56 Broken Kindle Screens_ is a print on demand paperback that consists of found photos depicting broken Kindle screens. The Kindle is Amazon's e-reading device which is by default connected to the company's book store.

The book takes as its starting point the peculiar aesthetic of broken E-Ink displays and serves as an examination into the reading device's materiality. As the screens break, they become collages composed of different pages, cover illustrations and interface elements.

Aether9
------

![](product-shot-01.jpg)

* Design: **OSP** (Gijs de Heij, Ludivine Loiseau, Pierre Marchand)
* Tools: **ConTeXt**, **Inkscape**, **OSPImpose**
* Year: **2012**
* Publisher: **Greyscale Press**
* [http://greyscalepress.com/2012/books/aether9/](http://greyscalepress.com/2012/books/aether9/)

_Aether9_ is a collaborative adventure exploring the danger zones of networked audio-visual live performance. The catalogue was generated from the collection of images, mailing-list and chat interactions that developed between 2007 and 2010. The book was designed during an intense sprint and some aftershocks in collaboration with the Open Source Publishing team.

This is a Book with an Attitude because it allows readers to follow the process, behind-the-scenes discussions, technical pitfalls, perils of networked communication and script development not only of the performances but also of the development of the book itself.

In an interview included in the catalogue, OSP-members Ludi Loiseau and Pierre Marchand discuss whether the Python scripts and source code could be realistically re-usable for other publication projects.

LL: The documentation part is still on the to-do list.

Yet a large part of the code is quite directly reusable. The code allows to parse different types of files. E-mails and chat-logs are often found in project archives. Here the python scripts allows to order them according to date information, and will automatically assign a style to the different content fields.

PM: The code itself is a documentation source, as much on concrete aspects, such as e-mail parsing, than on a possible architecture, on certain coding motives, etc. And morst importantly, is consists in a form of common experience.

Q: Do you think you will reuse some of the general functions/features of archive parsing for other projects ?

PM: Hard to say. We haven’t anything in perspective that is close to the aether9 project. But for sure, if the need of such treatment comes up again, we’ll retrieve these software components.

LL: Maybe for a publication/compilation of OSP’s adventures.

Balsamine
-----

![](images/bcc-lacarteouleterritoire.gif)

* Publisher: **Balsamine**
* Design and development: **OSP**
* Tools: **Etherpad**, **text-editor**, **chrome browser**
* Year: **2013**
* [http://git.constantvzw.org/?p=osp.work.balsamine.2013-2014.git](http://git.constantvzw.org/?p=osp.work.balsamine.2013-2014.git)

css to print

La Carte Ou le Territoire
----

![](images/bcc-lacarteouleterritoire.gif)

* Design and development: **Stéphanie Vilayphiou**
* Tools: **?**
* Year: **2012**
* Publisher: **?**
* [http://bcc.stdin.fr/LaCarteOuLeTerritoire](http://bcc.stdin.fr/LaCarteOuLeTerritoire)

In The map or the territory Stéphanie Vilayphiou selected a controversial book, Michel Houellebecq’s _The map and the territory_, which became renown for its evident quotes from Wikipedia that were not acknowledged by the author nor by his publisher. She took the book's digitized text and wrote a software filter, which looks for each sentence (or part of it) in Google Books, finding the same sequences of words in other books. Visually the book transforms then in a digital collage of quotations (whose context is maintained in the background), loosing even the last bit of originality. 
Vilayphiou embodies her sharp irony within a functional mechanism, exploiting Google’s industrial collection of texts and smartly expanding the mediating properties of language through the networks. 

Considering your tools: a reader for designers and developers
----

* Editing and design: **Alex Leray**, **Stéphanie Vilayphiou**
* Tools: **?**
* Year: **2013**
* [http://reader.lgru.net](http://reader.lgru.net)

This publication contains newly commissioned, translated and re-issued texts, distributed over 5 chapters: 

0. _Discrete gestures_ (about how our body is informed by our digital tools) 
0. _Reading interfaces_ (a different approach to computer literacy)
0. _The making of the standards_ (about the social and technical processes behind the elaboration of norms)
0. _Myriadic composition tools_ (tackles the question of software as a cultural object through the lens of digital typography)
0. _Shaping processes_ (methodologies for open and critical collective practices)

Readers and writers are invited to  re-publish, re-distribute, re-write and translate the texts contained in this reader.

The Death of the Authors
----

* Design and development: **An Mertens**, **Femke Snelting**
* Tools: **ConText**, **Python**
* Publisher: **Constant**
* Year: **2012**
* [http://publicdomainday.constantvzw.org/1941.php](http://publicdomainday.constantvzw.org/1941.php)

Every year on New Year's Day, due to the expiration of copyright protection terms on works produced by authors who died seven decades earlier, thousands of works enter the public domain - that is, their content is no longer owned or controlled by anyone, but it rather becomes a common treasure, available for anyone to freely use for any purpose. 

On the 1st January 2013 we warmly welcomed the works of a.o. the Brussels' author Neel Doff, the feminists Germaine Dulac, Violet Hunt and Tina Modotti, the Japanese poet Akiko Yosano, the sf-writers Ernest Bramah, Alexander Beliaev and Nictzin Dyalhis, the novelists Robert Musil, Roberto Arlt, Stefan Zweig and Bruno Schulz, the poets Olena Teliha and Jakob van Hoddis, the painter Walter Sickert and futurist poet Daniil Kharms ... 

Department of Reading
----

![](DOR_TheAuthorAsGesture.png)

* Design and development: **Sönke Hallman**, **Michael Murtaugh**
* Tools: **IRC**, **mediawiki**
* Publisher: **The Department of Reading**
* [http://automatist.net/deptofreading/wiki/pmwiki.php/TheAuthorAsGesture](http://automatist.net/deptofreading/wiki/pmwiki.php/TheAuthorAsGesture)

The Department of Reading Internet System (DORIS) consists in the conjunction of a wiki with a chatbot. The two systems form complementary reading/writing spaces; Unlike many "multi-media" attempts to merge the interfaces in a seamless whole, the separation between wiki and chat is embraced as each reinforces different aspects of reading and writing and are meant to be experienced in concentration and in isolation of the other. 

While the wiki is ostensibly a software designed for collective writing, and IRC a space for dialogue/forum, the Department of Reading aims to explore a somewhat different aspect of sociability: namely to slow down and explore the very acts of reading and writing in a group.

Diffly
----


* Design and development: **James Somers**
* Year: **2011**
* [http://diffly.heroku.com](http://diffly.heroku.com)

James Somers about _Diffly_: 

"I wanted a tool that made it really easy to give feedback in a visually intuitive way. It only needed to support (a) deletions, (b) insertions, and (c) comments. I wanted it to generate the visuals automatically as you made changes to the original text."

How it works: 

 Paste your original text into the copyediting area. 
 When you're ready to start making changes, hit "mark start." 
 There are three basic changes you can make: insertions, deletions, and comments. 
 To make a comment, just highlight some text you want to comment on and hit the "[" key. It'll surround your selection in brackets. Put your comment in the parentheses that follow. 
 Click the "finish" button to get a link to a page with the final marked-up version. 

L’Ève future – Spécimens de fontes libres
----

* Editing: **Auguste de Villiers de L’Isle-Adam**, **Manuel Schmalstieg** et al. 
* Design: **Patricio André**, **Aurélien Barrelet**, **Claire Bonnet**, **Sophie Czich**, **Fabio Da Cruz**, **Jessica Friedling**, **Justine Ludi**, **Laurent Monnet**, **Noémie Pasqual**, **Lucas Selhane** et **Laura Wohlgehaben**. 
* Year: **2013**
* Publisher: **Greyscale press**
* [http://greyscalepress.com/2013/books/eve-future-specimens-de-fontes-libres/](http://greyscalepress.com/2013/books/eve-future-specimens-de-fontes-libres/)

Free and open-source fonts displayed in recent years a great vitality, thanks to their entry in web browsers and the development of online services like Google Webfonts.
The variety of these fonts was the trigger for this book was developed during a workshop at HEAD (Haute Ecole d'Art and Design in Geneva) in February 2013.

Directed by students of visual communication courses, this book presents more than two hundred specimens of free fonts to examine and compare their characteristics for different body text.
While aiming to create a directory for typographical, this catalog also runs as a novel, because this entire "L'Eve Future". This story of science fiction, published in 1886 by Auguste Villiers de L'Isle-Adam, offers a wide variety of typographic signs and offers an excellent field for the game characters.

Exquisite Code
-----

![](images/maggot_sw_fin_1.jpg)

* Design, development, writing: **Brendan Howell**
* Year: **2010**
* [http://exquisite-code.com/](http://exquisite-code.com/)

exquisite_code is an algorithmic performance system for heterogeneous groups of writers.

The writers are fed a sequence of prompts on short timed intervals (6 - 20 minutes), supervised by a Proctor.

The core of the system is a piece of software which accepts multiple chunks of text for modification and combination into a single linear narrative.  The work is informed by the Surrealist "Exquisite Corpse", William Burroughs' "Cut-Up" and Oulipo methods.  The edit machine worms its way through the bodies of disparate texts, cutting, moving and replacing words.  Like non-automatic editing, the redaction process is iterative, where modified texts are sometimes returned to the author for further refinement.  The edit-worm may frustrate writers but it produces an output that would not be possible in a system of complete authorial control.

The software consists of a web-based application which is coded using open source frameworks written in the Python programming language.  The text manipulation algorithms are based on research in Natural Language Processing algorithms and statistical analysis.

Flossmanuals: Inkscape
---

![](images/inkscape.jpg)

* Editing: Joshua Facemyer, adam hyde, Cedric Gemy, Elisa de Castro Guerra, Alexandre Prokoudine, John Curwood, Nevit Dilmen, Gondouin-Liu, Rafe DiDomenico, Austin Martin 
* Tools: Inkscape, Booki
* Year: 2008, 2010
* Publisher: FLOSS-manuals
[http://en.flossmanuals.net/inkscape/](http://en.flossmanuals.net/inkscape/)
* Note: _Just one of many excellent manuals published by FLOSS-manuals_

FLOSS Manuals is a collection of manuals about free and open source software. Our core aim is making it easy to maintain and contribute to manuals, text books, teaching materials by providing an easy to use interface for collaborating on the creation of texts about Free Software.

Contributors include designers, readers, writers, illustrators, free software fans, editors, artists, software developers, activists, and many others. Anyone can contribute to a manual – to fix a spelling mistake, add a more detailed explanation, write a new chapter, or start a whole new manual on a topic.

Funzie fonzie Leesmasjien
----

* Hardware: Michael Korntheur
* Software: Pierre Marchand
* Tools: Fonzie
* Year: 2012

Initiated by Michael Korntheuer, the neighbours of Hackerspace Brussels constructed a DIY-bookscanner. During VJ13 the machine will be hosted in Constant Variable. It will be operated through Fonzie, a speculative software developed by Pierre Marchand, that converts physical to digital reading.

The reading process of the machine can be watched, understood, read, rewritten, changed and endlessly executed. This worksession explores the artistic potential of the different elements in the transmission from the physical book to the digital object: the code, the fonts, the training data, the book and the digitally born text. 

Librivox: Stops, or How to Punctuate
------

* Text: Paul Allardyce
* Reading: Zachary Brewster-Geisz, Laurie Anne Walden
* Tools: Audacity
* Year: 2008
* [http://librivox.org/stops-or-how-to-punctuate-by-paul-allardyce](http://librivox.org/stops-or-how-to-punctuate-by-paul-allardyce)
* Note: _Just one sample of the many excellent books-with-an-attitude to be found at LibriVox_

LibriVox volunteers record chapters of books in the public domain, and then we release the audio files back onto the net for free. All our audio is in the public domain, so you may use it for whatever purpose you wish.

Volunteering for LibriVox is easy and does not require any experience with recording or audio engineering or acting or public speaking. All you need is a computer, a microphone, some free recording software, and your own voice. We accept all volunteers in all languages, with all kinds of accents. You’re welcome to volunteer to read any language you speak, as long as you can make yourself understood in it. You don’t need to audition, but we do suggest a 1-Minute Test recording just to check your setup. We’ll accept you no matter what you sound like. We get most of our texts from Project Gutenberg, and the Internet Archive hosts our audio files (for free!).

The Public Library
-----

* Design and development: **Marcell Mars** 
* Tools: **Calibre**, **letssharebooks** 
* [http://www.memoryoftheworld.org/public-library/](http://www.memoryoftheworld.org/public-library/)
* Note: _e-library with an attitude?_

In the catalog of History the Public Library is listed in the category of phenomena that we humans are most proud of. Along with the free public education, public health care, scientific method, Universal Declaration of Human Rights, Wikipedia, Free Software ... It is also one of those almost invisible infrastructures that we start to notice only once they go extinct. 

Internet, however has overturned what we take as given and as possible. The dream of all people getting access to all knowledge suddenly came within our reach. However, the actual trajectory of development of public libraries in the age of internet are pointing in the opposite direction: Public libraries now cannot receive, and sometimes not even buy, the books of some of the largest publishers. The books that they already hold must be destroyed after lending them 26 (?!?) times. And they are losing the battle to the market dominated by new players such as Amazon, Google and Apple. 

Welcome to the Public Library!


The SKOR Codex
-----
 
* Design and development: **La Société Anonyme** 
* Tools: **?**
* Year: **2012**
* Publisher: **SKOR**
* [http://societeanonyme.la/#codex](http://societeanonyme.la/#codex)
 
The SKOR Codex is a printed book which will be sent to different locations on earth in the year 2012. It contains binary encoded image and sound files selected to portray the diversity of life and culture at the Foundation for Art and Public Domain (SKOR), and is intended for any intelligent terrestrial life form, or for future humans, who may find it. The files are protected from bitrot, software decay and hardware failure via a transformation from magnetic transitions on a disk to ink on paper, safe for centuries. Instructions in a symbolic language explain the origin of the book and indicate how the content is to be decoded. La Société Anonyme noted that "the package will be encountered and the book decoded only if there will be advanced civilizations on earth in the far future. But the launching of this 'bottle' into the cosmic 'ocean' says something very hopeful about art on this planet." Thus the record is best seen as a time capsule and a statement rather than an attempt to preserve SKOR for future art historians. The SKOR Codex is a project by La Société Anonyme.

Univers Else
-----

universelse_B.png

* Design and development: OSP, Speculoos 
* Tools: Fonzie, potrace
* Year: ?
* [http://ospublish.constantvzw.org/foundry/univers-else/](http://ospublish.constantvzw.org/foundry/univers-else/)

Univers Else is an experiment, a first attempt to escape the post ’80 era of geometrical purity that is so typical of Postscript vector based font drawing. The shapes of Univers Else were obtained from scanning printed textpages that were optically composed by cheap phototypesetting machines in the sixties and seventies. Some of Univers Else beautiful features are: round angles, floating baselines, erratic kerning. 

More precisely in this case, George Maciunas of the Fluxus group used an IBM composer (probably a Selectric typewriter) for most of his own work, and as a former designer, for all Fluxus work. In the 1988 book ‘Fluxus Codex’, kindly given to Pierre Huyghebaert by Sylvie Eyberg, the body text is typeset in a charmingly rounded and dancing Univers that seems to smile playfully at its dry swiss creator. As if it was really tempted, trying to provide a beautiful warm up to this old modernist classical. Along with a free, open and libre version! 

A book about contemporary architecture in Wallonia and Brussels provided an ass-kicking opportunity. Three weights were needed. We were not sure that we’ll have the time to finish the fonts in time before having to send the pdf file to the printer. So we laid out the publication with the standard Univers family, and replaced it at the last moment with our version. Suddenly, the 286 pages book jumped out of the ice! 

Different scans were assembled by Grégoire Vigneron following different grids. These huge bitmaps were processed with appropriate potrace settings by the Fonzie software through a .ufo font format as a working format, and an OpenType as output. Some testing and fine-tuning was done by Pierre Marchand, Delphine Platteeuw and Pierre Huyghebaert in FontForge and the font was ready, in a finished state enough to typeset the book. The oblique versions was simply slanted on the fly. 

The Weise7 in/compatible Laboratorium Archive 
----

![](images/weise7-book-home5.jpg)

* Editing: **Kristoffer Gansing**
* Design and development : **Bengt Sjolen**
* Tools: **OpenWRT GNU/Linux**, **InDesign** (?)
* Year: **2013**
* Publisher: **Weise7**

The Weise7 in/compatible Laboratorium Archive is a record of the Weise7 Studio show for Labor Berlin 8, a series of workshops run at Weise7 and its own creation.

Unlike other books, this book acts as an Internet independent wireless server, running from a tiny, custom designed computer inside the book.

To start the server just open the book and connect to it wirelessly using your laptop, tablet or phone. Browse the contents in a park, library or while half way up a mountain, sharing the content with those in your vicinity. Inside can be found high resolution images of our exhibition, video, source code and texts about each of the works. To turn off the server, simply close the book.

The Weise7 in/compatible Laboratorium Archive was launched on March 9th at Labor Berlin at the Haus der Kulturen der Welt, Berlin, and was 5 months in development.

hosts images, video, source code, schematics and texts covering all aspects of the exhibition, workshop and book production, served to your browser directly from our book. 

The Weise7 in/compatible Laboratorium Archive is served to you directly from a tiny computer, designed especially for this book by studio partner Bengt Sjolen.

The board runs the OpenWRT GNU/Linux distribution, customised for this device. The entire source code for this distribution is available in the archive. Full schematics of the board are also available, making it possible for others to customise and manufacture their own version of this unique device.

e-books HEAD
----

* Year: **2014**

e-books ERG
----

* Year: **2014**

