#! /usr/bin/env bash

pandoc -f markdown -t context -o bookswithanattitude.tex --chapters bookswithanattitude.md
# sed -i 's/here,nonumber/force,nonumber/g' bookswithanattitude.tex
context template.tex --result=bookswithanattitude.pdf
