 ~~In the current climate of cross-
media publishing, they are useful for beautifying stylesheets, which
disconnects bookdesign from an approach that considers the intertwining of
form, content and context, relating to materiality as its core qualities.
[expended later]~~

 (idea of degradation does not map to bookdesign)
--------------------------------------------------------------


(epub is not the same as styling content)
----------------------------------------------------------------


[http://my.opera.com/ODIN/blog/2009/10/12/how-media-queries-allow-you-to-
optimize-svg-icons-for-several-sizes](http://my.opera.com/ODIN/blog/2009/10/12
/how-media-queries-allow-you-to-optimize-svg-icons-for-several-sizes)

Balsamine - css3 to print
-------------------------

Balsamine (css to print)

frustrated by lay-out programmes; html well documented, immediate visual
feedback.

The printed programme for the season 2013/14 of the Balsamine theater was
build with a web browser, using the built-in "print to file" functionnality to
produce the final object. It is an unusual, unorthodox method to create
printed matter (published in offset printing) at this level of articulation.
Not only the publication is lengthy and rich in terms of design elements
(typefaces, styles, images...), but it also shows some possibilities of
flexible/reflowable/contextual design at the level of page layout.

The initial idea came out of the excitement for the richness of layout
possibilities offered by CSS, that goes beyond the static positioning offered
by WYSIWYG editors. Fondamentaly, they where in another kind of layout. Blocs,
inline elements, floating capabilities from CSS2 are now joined by even more
innovative models such as flex-box and [...]. HTML+CSS+JS also differs from
procedural layout programs such as LaTex or ConText as it proposes multiple
ways to author the objects (visually, programmatically, with text etc.) and is
well documented. HTML and CSS allows for static and reflowable design.

The process itself was collaborative, making use of the distributed nature of
the web (a page can point to external stylesheets, scripts and assets) in a
way that doesn't widen the labor division but rather minimize it by enabling
the different designers and programmers to take part in the overall design at
different levels. This was done by using, for instance, collaborative text-
editors (etherpad) to design together stylesheets and the page structures.
beyond the document model, the web is also an environment that allow for
buidling a framework with just the same technologies; just display:none to
hide the buttons.

Starting for the specifities of the printed publication, the designers then
adapted the brochure to the web, creating a derivative rather than an
parralele output

CSS3

* different positioning offered - floating, chainable blocks  
* combining textflow with flowing lay-out 
* mixing positioned and flowing elements 

  
For CSS3 to work on e-readers…

Also including print!

semi-flexible lay-out



Making use of affordances of specific devices, design hooks
-----------------------------------------------------------

In what ways are devices different?

-emphasis on screens, technical quality of screens: media queries, stylesheets for tty, tv, projection---for CSS2, now with CSS3 media future, widths, midwidths, maxwidths, heights, aspect ratio, color, color index, monochrome---max monochrome.

why media queries, recent development in CSS in wave of responsive design
because---these are the tools through which you build response. show limits of
response--what are those limits?

keeping a log of our experiment.

A broken e-book made us think about taking battery life into the bookdesign.
Another physiclity of reading is produced by the limited autonomy of the book.

Reflow smaller type so less refresh?

Going out with a bang - what would be the last event - going very bright

Summary style, bullet points then keywords

specific book and battery life - depending on the length of the book?

battery in relation to the way to go until the end of the book. The longer the
book is, the faster you have to go to reach the end.

PAPER BOOK: Binding, paper choice, cover stock, bladspiegel, typography,
printing, how and where illustrations are placed

E-BOOK: suggested styling, relative sizes (if font is changed), mark-up, cover
design, Other type of writing, structures, amount of characters book,
navigation, multimedia elements

websites content is written with web in mind. Different kind of navigation
possible.

ebooks are subordinated to the way of writing books, but physical books are
until now the best devices to read linearbooks.

if it breaks down it becomes a different book, not only on the level of design
but also on the level of writing

  
one website for 5 devices??? or 5 websites for 5 devices?

they are related but. where is the cut-off?

mutable design

the browser on which you look at a website matters.

current trend is to polish the differences -> continuity

reeading a book on your e-ink device should be different: working from
discontinuity

acid test: what if you would turn the compliance result into a design-tool

where it breaks is also where it becomes specific

see images at
[http://fr.wikipedia.org/wiki/Acid3](http://fr.wikipedia.org/wiki/Acid3)

working with difference rather than against.

technologies:

  
mobile vs desktop

  * •screen 
  * •  size 
  * •  resolution 
  * •  density 
  * •  color depth 
  * •  surface (matt / glossy etc.) 
  * •  refreshing rate (70 per s. vs manual refreshing) 
  * •  pixel arrangment (grid vs scattered) 
  * •connected/not connected 
  * •localized (gps /ip address/proximity: close to another device, close to your face etc.) 
  * •orientation 
  * •processing power 
  * •battery 
  * •script aware 
  * •keyboard, user input devices 
  * •microphone 
  * •speakers  

"reality is infinite, what level of reality are we dealing with?" -- Lilly

speed of reading

the systems knows about its remaining "life".

Duration of reading

Freeze

A sense of it's own making, what edition are you reading; license;
------------------------------------------------------------------

reclaiming read-write culture.

 "The distinctive value of ebooks is orthagonal to the value of paper books,
and it revolves around the mix-ability and send-ability of electronic text.
The more you constrain an ebook's distinctive value propositions — that is,
the more you restrict a reader's ability to copy, transport or transform an
ebook — the more it has to be valued on the same axes as a paper-book."

– Cory Doctorow, [http://www.gutenberg.org/cache/epub/11077/pg11077.html](http
://www.gutenberg.org/cache/epub/11077/pg11077.html)

Community contributed designs for specific books, illustrations.

"what edition are you reading?" Mixing styles.

Active archives modes – collage

different editions -- compare diffs


BROL
====


What do we plan to do?

to experiment with a responsive design that starts with the notion of
difference btn devices versus trying to repair. to play with the fact that
these dveices are different rather than trying to hide, or keep unexposed the
differences. for example, early acid tests for browser: the relation is made
betwen earl browser wars when it was about lack of compatability, how diff
pages would be rendered. than designrs began developing tests for browsers--
make it difficult for browser to render something which would reveal if
browser was compatable with standard. imagination of what could be the same,
what could be wrong. the test a priori assumes difference---aim is to come
into compaliance. imagination of similarity. but potential is tha every
browser renders something else. you can say they failed the exam, but also say
that they invented something. so the experiment for flatland is to make three
difference experiences of the same text.

encounter---how the document, the epub document encounter. don't want to limit
discussion to devices? diff devices = diff reading situations. pocketbook
versus coffeetable books. part of the design intention to design differently,
not only because of diff materiality but also different reading situations.
context includes more than devices. hiding in immersive design? through
measuring position and tilt, these kinds of considerations is to make a device
feel more "natural"--that a screen can have a sensor of lights an adjust the
backlighting of a screen so that you don't notice that it's getting dark.
calibration is to (~=)naturalize experiences [other word?]---devices-in-
denial-mode. so responsive design is actually to deny human response. more
about device invisibility.

So actually... why do we really need e-pub and not html?

concern about consistency, atonomous object
Proposes an order (chapter, toc)
does not reprsesent all the books (eg; works well for novels, what about concrete poetry?)
basically organizes HTML in a way that fits the idea of a book
what's the obsession with X-html: some say efficiency: XML is lighter to interpret
user's voice: drm free concern
could become integrated to the web

~~WHAT DO YOU MEAN accessibilities/search engines etc.~~


Devices and Denial, an existential crisis
=======================



Pro
---

* self-contained package, a bundle so transportable, move it accross devices online-offline 
* optimized for current reading devices 
* metadata (but very publisher-oriented). Could have more design elements 
* substainable, cross-plateform, uuid 
* notion of order, chapters, navigation 
* pagination: although limited, a way to sense the progression/length of a
  book 
* semantic markup ? 
* openness: the old flash discussion, how can we make epub more interesting for designers 

Cons
----

* read-only :/  
* "A key concept of EPUB is that content presentation should adapt to the User
  rather than the User having to adapt to a particular presentation of content"
  very limited understanding od "adapting to the user" 


* * * * * * * * * * 

- "living books", or how can a (even physcal) book connect to its derivates, to reviews and opinions, to inline comments... to that "digi,.mn,.mn,.mn.,tal soul" they may have
- related to de previous one: "the never finished book/text/whatever". Ho can we connect versions of printed and digital books, how can we deliver updates to the readers of a previous version, how can we visualize those differences.
- backwards attribution: how to notify the author of derivative works? is there a way of tracking the attribution chain back and forth?
- visualizing derivative works, remixes, quotes, editions, issues, etc. in general so that we can clearly navigate through them and understand their relationships.
