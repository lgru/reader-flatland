% Ebooks With an Attitude -- A pamphlet
% Femke Snelting, Alexandre Leray

[TOC]

## Introduction

> The format is intended as a single format that publishers and conversion
> houses can use in-house, as well as for distribution and sale.
>
> -- From the ePub definition <!-- exact source/link? -->

Books are no longer individually designed paper objects. In an environment of
federated publishing, they are published in different formats such as ePubs
--the official standard for digital books-- but also as HTML pages and PDF
documents. Books are read in e-readers as well as in web browsers; printed at
home or delivered as discrete files that require their own software for
rendering. The ontological stability of books is cut across myriad standards,
formats and devices. In addition to being embodied in different hardware and
software platforms, books are also edited in discontinuous ways where source
material and modifications may come from different places, at different
moments. The book is now a fractious environment and invites new questions of
first locating design amid such disjointed ecologies, but also to think about
what opportunities for design open up when the final output is no longer a
stable object.

<!-- official standard ? -->

![](images/responsive-wordpressdotcom-e1358480556937.jpg)

This assemblage view of book design contrasts with extant understandings of
design and the subsequent division of labor in design work. In current
understandings of ebookmaking, we find a grossly simplified division where, on one hand, publishing
entrepreneurs, device makers, and "users" (formerly known as "readers") are actively involved in selling, buying, developing and consuming technology; and on the other hand, designers are busy with print. In this configuration, the designer turns into a caricature of a nostalgic being, always longing for a time when books were still uniquely shaped
single-master artefacts. Designers by this caricature are always desirious of pixel perfect specifities if not of cold glue binding and the smell of fresh ink on woven paper.

Federated  publishing is a reality. Strangely, the way designers (can) work
seems more formatted than ever, and we believe the technical infrastructure
that we see emerging, is part of this problem. How can we make the fact that design is embedded in chains of
dependencies into an asset? How can we prevent design and artistic practices
from being split apart from other realities? How can we rethink design as part of a collective of algorithms, standards and tools?

## The ePub standard

> We invite representatives from following communities to submit papers:
> Publishers digital books production (including magazines and journals),
> Retailer/Bookshops (on line and off line), Manufacturers of eBooks readers
> devices, Providers of eBooks readers applications software, Providers of
> eBooks authoring tools, eBook related software contributors to browser cores,
> Distributors (eBook server), News agencies, Libraries, Search engines,
> Specialized search and web analytics, Library management software companies,
> Relevant standard setting institutions, eBooks service providers, Cloud-based
> eReading platform providers, Accessibility companies and institutions,
> Localization companies, Consumers, Academic researchers in digital
> publishing, Industry consultants
> 
> -- [eBooks: Great Expectations for Web Standards](http://www.w3.org/2012/08/electronic-books/expected.html)

Around the table at a meeting of the International Digital Publishing forum, we can find several representatives discussing the state of the ePub format. Publishers want to speed up book production by interactively connecting stock management to content delivery in an overall effort to create reliable reflowable output across myriad devices and platforms. Eager device vendors are trying to be just compatible enough to cut off their competition. While disability rights groups pressure for accessible content. At the same table, [leading global type providers promise great reading experiences](http://idpf.org/epub/30/wg-charter). There are no book designers at this table.

Today's largest effort in defining a common technical infrastructure for digital books has cristalized
in the ePub format. While it is still limited in terms of design possibilities, its development and rapid adoption is encouraged by the historical success of HTML and driven by the rapid adoption of- and popular excitement around HTML5 and CSS3.

Open Standards like the ePub format are importants to us on many levels.
Without wanting to deny the various forms of power play that can take place, 
Open Standards favor a read/write culture by exposing its own construction. 
Rather than being fixed in stone, they are continuously refined
through practice and feedback. This is usually done with a concern for 
continuity, helping to extend the lifespan of digital objects and avoiding
programmed obsolescence. Open Standards also help preventing dependency on a
single company (hardware, marketing, authoring tool), that would define the
format after its own political agenda and commercial interests. Open Standards create the building blocks for a rich ecology of practices, and in doing so build paths between practitioners, devices, workflows and software.

The ePub standard offers potentially a platform for the many different actors involved in the creation of digital books and allows them to acknowledge interrelated concerns at the various stages of the life of a book: writing, designing, distribution, reading, etc. Unfortunately, the stakeholders involved so far have merely been actors on the level of infrastructure. It is a first clue that the ePub view on digital book production is rather narrow.

## High Fidelity

> Think about how challenging that is. As I mentioned in a meeting recently,
> it’s like trying to create a totally immersive, 3D movie and have the same
> product gracefully degrade for playback on an AM radio station.
> 
> -- Joe Wikert, [Graceful eBook Degradation](http://toc.oreilly.com/2012/08/graceful-ebook-degradation.html)

In discussions about finding a workable standard for ebooks, the concept of "graceful degradation" has taken center stage. It is the idea that a system can continue to function in the event of the failure of one or more components. This principle is contrasted with "naive" systems that are designed in such a way that they would break down if only a single condition would not be met. Take for example how a text if necessary can still be shown in a default font while it was designed with a bespoke typography. Images can be displayed in full-color on devices that support it, and shown in black and white on those with one-bit output, or simply omitted altogether. The designer designs for the optimal situation of having all features at her disposal, and tries to anticipate what would happen on "less capable" devices.

Following the logic of graceful degradation, a lack of color can only be seen as a fault, as an absence. However the distinct graphic character of line-art is also a quality. The display of a "book" on a color device, that was originally designed for a one-bit screen can actually be considered a significant reduction, a form of "graceless optimization" when qualities of design are conflated with high fidelity, and technical complexity is seen as top end. An all too linear approach to degradation aligns with what vendors want from hardware: more is better, and larger is greater. 

Current standards such as ePub and other flavors of XML tell a story through their particular idea of optimization. Standards and file formats for this type of books are mainly developed and pushed for and by industry, for whom the idea of optimal is rather “economic”.  As a result, the range of design options is quickly limited to the lowest common denominator: one-column text layouts without possibility to think about the relation between page size and resolution, margins, the amount of text per page, etc. In the development of the ePub standard certain aspects such as metadata and fluidity of the distribution accross devices were prioritized. Omitting the most basic elements of book design reflects a technocratic rather than a cultural approach to "books". It subsequently forces that same approach onto ideas of authorship, distribution and ultimately onto designers' tools and practices.

## Form, content and situation

> We want to dispel the myth that digital books can't also be crafted works of
> visual design. Just as web design has evolved and matured, so too will ebooks,
> and book designers have a new medium available in which to express their
> creativity.
> 
> – [http://epubzengarden.com/about](http://epubzengarden.com/about/)

Just like XHTML and CSS, the ePub standard operates from the assumption that for creating a digital book, a separation of "content" and "styling" is necessary. The division of "semantics" from "presentation" has useful implications: it allows for instance for the reuse of styles across multiple pages and to refine a design in one go. But it is also problematic in the sense that it tends to divide labor and format tasks, ultimately leading to conceptions of design and authorship as independent of their material context.

Fortunately, in current practices of webdesign, designers are no longer confined to Photoshop mockups but actively engage with writing HTML, CSS and Javascript code themselves. They are hacking their way across the divide between content from style, while engaging with technological infrastructures. This shows that designers do find ways to circumvent this unfortunate separation. It suggests that other approaches to electronic book design might be possible.

Paul Rand once said "Design is the synthesis of form and content" [^Rand1] and that "A work of art is realized when form and content are indistinguishable" [^Rand2]. From Peter Behrens [^Behrens] to Jan van Toorn [^vanToorn], a century of design achievement shows that content and form cannot that easily be isolated from each other, and that separating them as a principle deprives objects from a substantial relation. But Rand, Behrens and van Toorn were not just working on form and content, they were thinking about their designs with specific reading situations in mind, carefully making choices about paper, bindings or inks; experimenting with specific printing reproduction technologies. They were thinking in terms of design objects, bearing in mind how those would be handed over to the reader. From Gutenberg onwards, it is obvious how books have always been thought beyond content and form. They are embedded in a technological and economical context that in turn has an effect on form and content possible <!-- remove "possible" ?-->.

With the event of digital books, these entanglements have become even more apparent, and urgently necessitate a different paradigm when imagining the agencies of standards, practices and tools. From a sequence of bound printed sheets to electronic devices, digital books have turned into "book systems". Their *Gestalt* which was before achieved by having a full picture of the affordances offered by the printing press, is now depending on a variety of parameters determined by the formats (what is encoded and how?), the software (how is the data instantiated?), the devices (what are the specifications of the machine?) and even by the way the book is distributed.

Emerging web technologies actually start to hint at the fact that much more intelligent ideas of relations between form and device, or actually between form, situation and content, are possible and desirable. Even more important, designers are actively involved in developing work in this area. CSS media queries[^media-queries] are a first step in this direction, but they are mainly used for "responsive design", an approach to cross-platform design that aims to offer the same experience on different devices.

[^media-queries]:  Media queries is a technology that allows to detect some basic specificities of devices such as the screen size or the device orientation, and apply specific CSS accordingly. <https://developer.mozilla.org/en-US/docs/CSS/Media_queries>

Instead we are interested in "situational design"[^contextual-design]. We do not want to anihilate differences but rather work with them. We want a practice that is not only aware of the context in which it exists, but actively engages with it. It takes advantage of the affordances of specific situations, rather than trying to create a seamless experience. We are looking for intelligent layout systems where designers can collaborate with algorithms and devices, and play with the materialities of variable book systems. It would mean to consider templates and stylesheets not as strict normative containers but as situations that generate parameters for attraction and repulsion between the components of a design. They could take into account many parameters that are currently ignored, including connectivity, localization, battery level and so on. Rather than trying to control the flow of content, designers could find ways to dialogue with the various actors at work. Learning to love their idiosyncrasies and their attitudes.

[^contextual-design]: We decided to use the term "situational" because "contextual design" is being used for a type of design we are actually trying to avoid: <https://en.wikipedia.org/wiki/Contextual_design>.

[^Rand1]: <https://www.youtube.com/watch?v=51z-t7T0_6E>
[^Rand2]: <https://www.youtube.com/watch?v=4yOjts0tpco>
[^Behrens]: <http://www.design-is-fine.org/post/44774560571/peter-behrens-product-sheet-for-aeg-kettle>
[^vanToorn]: <https://vimeo.com/3775270>

## Books with an attitude

> Garson O'Toole says:        
> March 23, 2009 at 6:09 pm        
> 
> IMHO a fully successful ebook format should be integrated with the rest of the
> web. For example, major browsers like Firefox should be able to read it.
> [...] Links between ebooks and links in and out of ebooks should be
> supported. It would be wonderful if you could specify a link in an ebook or
> webpage that would take you to a precisely specified word sequence within
> another ebook. Links like this would allow the incorporation of ebooks into
> the general conversational structures of the web.
>
> -- http://www.teleread.com/ebooks/why-not-use-html-instead-of-epub/

To imagine the future of ePub away from the desire for coherence and pixel-perfectness is based on our understanding that in book publishing design <!-- remove "publishing" ?-->,
processes of production, authoring and distribution are deeply intertwingled, interlaced
practices. What then could an ebook that engages with content at
all levels,and celebrates the various attitudes? <!-- Weird structure, I don't understand what is the verb-->

For designers of books the territory of work has moved far away from the
layout of a single, specific output to the design of book systems that can in
turn produce multiple outputs/formats. Potentially, designers could become
participants in a more complex but interesting situation where they need to
negotiate media technology with a larger set of actors. The form of the book
would actively be defined through an amalgam of processes happening at
different moment and places in the process of making it, and not just be the
unmutable result of historical events.

We call for ebooks with an attitude!



An ebook with an attitude:

- opens up its own internal processes, tools, sources;
- has a sense of context, it considers itself as part of a whole
  chain/ecology of practices;
- aims to put reading and writing at the same level, or at least experiments
  with non-conventional divisions of roles;
- plays with reading and writing of both text and code; 
- treats technical content as part of a reading and writing experience;
- is networked and/or mutable;
- works with specific characteristics of specific devices/reading contexts,
  taking advantage of intrinsic qualities
  
  
Ultimately, the concept of "graceful degradation" means separating form and
content as a principle to be able to split the container from the content and
make form into style. 

<!-- HOW IN RELATION TO ABOVE: alienating design from technical materiality.-->

The hierarchies that are built into "fault tolerant" standards/fileformats for cross-media publishing. xxxx


In a time when the current format of ePub does not even start to compare with the
possibilities offered by browsers ten years ago, CSS Zen Garden remains
inspirational in the context of ePub. <!-- INTRODUCE CONTEXT--> CSS Zen Garden has indeed been a major
milestone in the adoption of HTML by designers by showcasing the benefits of
browser compliance to open standards as well as the flexibility of CSS. But it
has also lead to a reduction of design as mere styling. Although a traditional
print designer would arrange text, images and other symbols in relation to each
other to create meaning, the approach advocated by CSS Zen Garden tends to
confine the designer to cosmetic interventions.


Only very rarely does our practice reflect the rich intermingling between form, content and context that
opens up. 

There is a clear relation <!--EXPLAIN--> between a well supported standard and space for
design that we might recognize from the situation in 2000, when browsers would
each render webpages at will. Designers understood that a commitment to
web standards would allow them to be less dependent on browsers.



Library of ebooks with an attitude
=================================

<!--
An ebook with an attitude:

- opens up its own internal processes, tools, sources;
- has a sense of context, meaning considers itself as part of a whole
  chain/ecology of practices;
- aims to put reading and writing at the same level, or at least experiments
  with non-conventional divisions of roles;
- plays with reading and writing of both text and code; 
- treats technical content as part of a reading and writing experience;
- is networked and/or mutable;
- works with specific characteristics of specific devices/reading contexts,
  taking advantage of intrinsic qualities.
-->

The definition has as a consequence that the Library of ebooks with an
attitude is more than books. It also contains reading/writing systems and
sometimes even complete libraries. Your proposals are welcome at [lgru-reader@lists.constantvzw.org](mailto:lgru-reader@lists.constantvzw.org)


56 Broken Kindle Screens
------------------------

![](images/p1040699.jpg)

* Design and development: **Silvio Lorusso, Sebastian Schmieg**
* Tools: **?**
* Date: **2012**
* [http://sebastianschmieg.com/56brokenkindlescreens/](http://sebastianschmieg.com/56brokenkindlescreens/)

_56 Broken Kindle Screens_ is a print on demand paperback that consists of
found photos depicting broken Kindle screens. The Kindle is Amazon's e-reading
device which is by default connected to the company's book store.

The book takes as its starting point the peculiar aesthetic of broken E-Ink
displays and serves as an examination into the reading device's materiality. As
the screens break, they become collages composed of different pages, cover
illustrations and interface elements.

Aether9
------

![](images/product-shot-01.jpg)

* Design: **OSP** (Gijs de Heij, Ludivine Loiseau, Pierre Marchand)
* Tools: **Python**, **ConTeXt**, **Inkscape**, **OSPImpose**
* Year: **2012**
* Publisher: **Greyscale Press**
* [http://greyscalepress.com/2012/books/aether9/](http://greyscalepress.com/2012/books/aether9/)

_Aether9_ is a collaborative adventure exploring the danger zones of networked
audiovisual live performance. The catalogue was generated from the collection
of images, mailing-list and chat interactions that developed between 2007 and
2010. The book was designed during an intense sprint and some aftershocks in
collaboration with the Open Source Publishing team.

This is an "ebook with an attitude" because it allows readers to follow the
process, behind-the-scenes discussions, technical pitfalls, perils of networked
communication and script development not only of the performances but also of
the development of the book itself.

In an interview included in the catalogue, OSP members Ludi Loiseau and Pierre
Marchand discuss whether the Python scripts and source code could be
realistically re-usable for other publication projects.

> LL: The documentation part is still on the to-do list.
>
> Yet a large part of the code is quite directly reusable. The code allows to parse different types of files. E-mails and chat-logs are often found in project archives. Here the python scripts allows to order them according to date information, and will automatically assign a style to the different content fields.
>
> PM: The code itself is a documentation source, as much on concrete aspects, such as e-mail parsing, than on a possible architecture, on certain coding motives, etc. And most importantly, it consists in a form of common experience.
> 
> Q: Do you think you will reuse some of the general functions/features of archive parsing for other projects?
> 
> PM: Hard to say. We haven’t anything in perspective that is close to the aether9 project. But for sure, if the need of such treatment comes up again, we’ll retrieve these software components.
> 
> LL: Maybe for a publication/compilation of OSP’s adventures.



Balsamine
---------

* Publisher: **Balsamine**
* Design and development: **OSP**
* Tools: **Etherpad**, **HTML**, **less CSS**, **Javascript**, **chromium browser**
* Year: **2013**
* [http://git.constantvzw.org/?p=osp.work.balsamine.2013-2014.git](http://git.constantvzw.org/?p=osp.work.balsamine.2013-2014.git)

CSS to print


La Carte Ou le Territoire
----

<!-- change image -->
![](images/bcc-lacarteouleterritoire.jpg)

* Design and development: **Stéphanie Vilayphiou**
* Tools: **Python**, **HTML**, **CSS**, **Javascript**
* Year: **2012**
* Publisher: **?**
* [http://bcc.stdin.fr/LaCarteOuLeTerritoire](http://bcc.stdin.fr/LaCarteOuLeTerritoire)

In The Map Or the Territory Stéphanie Vilayphiou selected a controversial book,
Michel Houellebecq’s _The Map And the Territory_, which became renown for its
evident quotes from Wikipedia that were not acknowledged by the author nor by
his publisher. She took the book's digitized text and wrote a software filter,
which looks for each sentence (or part of it) in Google Books, finding the same
sequences of words in other books. Visually the book transforms then in a
digital collage of quotations (whose context is maintained in the background),
losing even the last bit of originality. 

Vilayphiou embodies her sharp irony within a functional mechanism, exploiting
Google’s industrial collection of texts and smartly expanding the mediating
properties of language through the networks. 


Considering your tools: a reader for designers and developers
-------------------------------------------------------------

* Editing and design: **Alexandre Leray**, **Stéphanie Vilayphiou**
* Tools: **?**
* Year: **2013**
* [http://reader.lgru.net](http://reader.lgru.net)

This publication contains newly commissioned, translated and re-issued texts,
distributed over 5 chapters: 

1. _Discrete gestures_ (about how our body is informed by our digital tools) 
2. _Reading interfaces_ (a different approach to computer literacy)
3. _The making of the standards_ (about the social and technical processes
   behind the elaboration of norms)
4. _Myriadic composition tools_ (tackles the question of software as a cultural
   object through the lens of digital typography)
5. _Shaping processes_ (methodologies for open and critical collective
   practices)

Readers and writers are invited to  re-publish, re-distribute, re-write and
translate the texts contained in this reader.


The Death of the Authors
------------------------

* Design and development: **An Mertens**, **Femke Snelting**
* Tools: **ConText**, **Python**
* Publisher: **Constant**
* Year: **2012**
* [http://publicdomainday.constantvzw.org/1941.php](http://publicdomainday.constantvzw.org/1941.php)

Every year on New Year's Day, due to the expiration of copyright protection
terms on works produced by authors who died seven decades earlier, thousands of
works enter the public domain -- that is, their content is no longer owned or
controlled by anyone, but it rather becomes a common treasure, available for
anyone to freely use for any purpose. 

On the 1st January 2013 we warmly welcomed the works of a.o. the Brussels'
author Neel Doff, the feminists Germaine Dulac, Violet Hunt and Tina Modotti,
the Japanese poet Akiko Yosano, the sf-writers Ernest Bramah, Alexander Beliaev
and Nictzin Dyalhis, the novelists Robert Musil, Roberto Arlt, Stefan Zweig and
Bruno Schulz, the poets Olena Teliha and Jakob van Hoddis, the painter Walter
Sickert and futurist poet Daniil Kharms… 

Department of Reading
---------------------

![](images/DOR_TheAuthorAsGesture.png)

* Design and development: **Sönke Hallman**, **Michael Murtaugh**
* Tools: **IRC**, **mediawiki**
* Publisher: **The Department of Reading**
* [http://automatist.net/deptofreading/wiki/pmwiki.php/TheAuthorAsGesture](http://automatist.net/deptofreading/wiki/pmwiki.php/TheAuthorAsGesture)

The Department of Reading Internet System (DORIS) consists in the conjunction
of a wiki with a chatbot. The two systems form complementary reading/writing
spaces. Unlike many "multi-media" attempts to merge the interfaces in a
seamless whole, the separation between wiki and chat is embraced as each
reinforces different aspects of reading and writing are meant to be
experienced in concentration and in isolation of the other. 

While the wiki is ostensibly a software designed for collective writing, and
IRC a space for dialogue/forum, the Department of Reading aims to explore a
somewhat different aspect of sociability: namely to slow down and explore the
very acts of reading and writing in a group.

Diffly
------

* Design and development: **James Somers**
* Year: **2011**
* [http://diffly.heroku.com](http://diffly.heroku.com)

James Somers about _Diffly_: 

"I wanted a tool that made it really easy to give feedback in a visually
intuitive way. It only needed to support (a) deletions, (b) insertions, and (c)
comments. I wanted it to generate the visuals automatically as you made changes
to the original text."

How it works: 

 Paste your original text into the copyediting area. 
 When you're ready to start making changes, hit "mark start." 
 There are three basic changes you can make: insertions, deletions, and comments. 
 To make a comment, just highlight some text you want to comment on and hit the "[" key. It'll surround your selection in brackets. Put your comment in the parentheses that follow. 
 Click the "finish" button to get a link to a page with the final marked-up version. 

L’Ève future – Spécimens de fontes libres
----

* Editing: **Auguste de Villiers de L’Isle-Adam**, **Manuel Schmalstieg** et al. 
* Design: **Patricio André**, **Aurélien Barrelet**, **Claire Bonnet**, **Sophie Czich**, **Fabio Da Cruz**, **Jessica Friedling**, **Justine Ludi**, **Laurent Monnet**, **Noémie Pasqual**, **Lucas Selhane** et **Laura Wohlgehaben**. 
* Year: **2013**
* Publisher: **Greyscale press**
* [http://greyscalepress.com/2013/books/eve-future-specimens-de-fontes-libres/](http://greyscalepress.com/2013/books/eve-future-specimens-de-fontes-libres/)

Free and open source fonts displayed in recent years a great vitality, thanks to their entry in web browsers and the development of online services like Google Webfonts.
The variety of these fonts was the trigger for this book developed during a workshop at HEAD (Haute École d'Art et Design, Geneva) in February 2013.
Directed by students of visual communication courses, this book presents more than two hundred specimens of libre fonts to examine and compare their characteristics for different body text.
While aiming to create a directory for typographical, this catalog also runs as a novel, as it features the entire "L'Ève Future". This story of science fiction, published in 1886 by Auguste Villiers de L'Isle-Adam, offers a wide variety of typographic signs and offers an excellent field for the game characters.

Exquisite Code
-----

![](images/maggot_sw_fin_1.jpg)

* Design, development, writing: **Brendan Howell**
* Year: **2010**
* [http://exquisite-code.com/](http://exquisite-code.com/)

*Exquisite_code* is an algorithmic performance system for heterogeneous groups of writers.
The writers are fed a sequence of prompts on short timed intervals (6--20 minutes), supervised by a Proctor.
The core of the system is a piece of software which accepts multiple chunks of text for modification and combination into a single linear narrative.  The work is informed by the Surrealist "Exquisite Corpse", "Cut-up", William Burroughs' and Oulipo's methods.  The edit machine worms its way through the bodies of disparate texts, cutting, moving and replacing words.  Like non-automatic editing, the redaction process is iterative, where modified texts are sometimes returned to the author for further refinement.  The edit-worm may frustrate writers but it produces an output that would not be possible in a system of complete authorial control.
The software consists of a web-based application which is coded using open source frameworks written in the Python programming language.  The text manipulation algorithms are based on research in Natural Language Processing algorithms and statistical analysis.

Flossmanuals: Inkscape
---

![](images/inkscape.jpg)

* Editing: Joshua Facemyer, adam hyde, Cedric Gemy, Elisa de Castro Guerra, Alexandre Prokoudine, John Curwood, Nevit Dilmen, Gondouin-Liu, Rafe DiDomenico, Austin Martin 
* Tools: Inkscape, Booki
* Year: 2008, 2010
* Publisher: FLOSS-manuals
[http://en.flossmanuals.net/inkscape/](http://en.flossmanuals.net/inkscape/)
* Note: _Just one of many excellent manuals published by FLOSS-manuals_

FLOSS Manuals is a collection of manuals about free and open source software. Our core aim is making it easy to maintain and contribute to manuals, text books, teaching materials by providing an easy to use interface for collaborating on the creation of texts about Free Software.
Contributors include designers, readers, writers, illustrators, free software fans, editors, artists, software developers, activists, and many others. Anyone can contribute to a manual – to fix a spelling mistake, add a more detailed explanation, write a new chapter, or start a whole new manual on a topic.

Hyperopia, your free encyclopedia memex thing
----

![](images/grab-book.jpg)

* Design and development: Robert M. Ochshorn
* Tools: Wikipedia, Python, nginx, couchdb, drift
* Year: 2014
[http://rmozone.com/snapshots/2014/02/hyperopia/](http://rmozone.com/snapshots/2014/02/hyperopia/)
* Note: _Developed as a gift for the CDG research group at MIT_

Hyperopia is a device based on the transitive principle that Reading is Writing and that if the whole is comprised of parts then the parts should stay connected to the whole: look at something closely enough and the truth of the universe is manifest in its every detail.
The book hosts a wifi network, which redirects all web traffic to its internally-held contents. Any website a user on the network attempts to reach will redirect to the book's UI, where they can create a new associative thread, or resume with an existing one. If multiple people are connected to the book at the same time, they can share a drift together. Links open inline and persist in a sidebar along with any text that is selected; the sidebar can be printed from the book as a receipt. In addition to displaying the contents of Wikipedia  (2012 edition), pages can be augmented with wikitext and attachments that will stick to the page.
Inspirations trace by way of the author's InterLace video studies back through the Memex, Project Xanadu, and The Wikireader, weaving around Hypertext, wikis, the World Wide Web, and with the deepest respects paid to Infogami.
And of course the project is uniquely enabled by Wikipedia. 


Funzie fonzie Leesmasjien
----

* Hardware: Michael Korntheur
* Software: Pierre Marchand
* Tools: Fonzie
* Year: 2012

Initiated by Michael Korntheuer, the Hackerspace Brussels constructed a DIY-bookscanner. During V/J13 the machine will be hosted in Constant Variable. It will be operated through Fonzie, a speculative software developed by Pierre Marchand, which converts physical to digital reading.

The reading process of the machine can be watched, understood, read, rewritten, changed and endlessly executed. This worksession explores the artistic potential of the different elements in the transmission from the physical book to the digital object: the code, the fonts, the training data, the book and the digitally born text. 

Librivox: Stops, or How to Punctuate
------

* Text: Paul Allardyce
* Reading: Zachary Brewster-Geisz, Laurie Anne Walden
* Tools: Audacity
* Year: 2008
* [http://librivox.org/stops-or-how-to-punctuate-by-paul-allardyce](http://librivox.org/stops-or-how-to-punctuate-by-paul-allardyce)
* Note: _Just one sample of the many excellent books-with-an-attitude to be found at LibriVox_

LibriVox volunteers record chapters of books in the public domain, and then we release the audio files back onto the net for free. All our audio is in the public domain, so you may use it for whatever purpose you wish.
Volunteering for LibriVox is easy and does not require any experience with recording or audio engineering or acting or public speaking. All you need is a computer, a microphone, some free recording software, and your own voice. We accept all volunteers in all languages, with all kinds of accents. You’re welcome to volunteer to read any language you speak, as long as you can make yourself understood in it. You don’t need to audition, but we do suggest a 1-Minute Test recording just to check your setup. We’ll accept you no matter what you sound like. We get most of our texts from Project Gutenberg, and the Internet Archive hosts our audio files (for free!).

The Public Library
-----

![](images/montage.jpg)

* Design and development: **Marcell Mars** 
* Tools: **Calibre**, **letssharebooks** 
* [http://www.memoryoftheworld.org/public-library/](http://www.memoryoftheworld.org/public-library/)
* Note: _e-library with an attitude?_

In the catalog of History the Public Library is listed in the category of phenomena that we humans are most proud of. Along with the free public education, public health care, scientific method, Universal Declaration of Human Rights, Wikipedia, Free Software… It is also one of those almost invisible infrastructures that we start to notice only once they go extinct. 

Internet, however has overturned what we take as given and as possible. The dream of all people getting access to all knowledge suddenly came within our reach. However, the actual trajectory of development of public libraries in the age of internet are pointing in the opposite direction: Public libraries now cannot receive, and sometimes not even buy, the books of some of the largest publishers. The books that they already hold must be destroyed after lending them 26 (?!?) times. And they are losing the battle to the market dominated by new players such as Amazon, Google and Apple. 

Welcome to the Public Library!


The SKOR Codex
-----

![](images/codex-open-3.png)

* Design and development: **La Société Anonyme** 
* Tools: **?**
* Year: **2012**
* Publisher: **SKOR**
* [http://societeanonyme.la/#codex](http://societeanonyme.la/#codex)
 
The SKOR Codex is a printed book which will be sent to different locations on earth in the year 2012. It contains binary encoded image and sound files selected to portray the diversity of life and culture at the Foundation for Art and Public Domain (SKOR), and is intended for any intelligent terrestrial life form, or for future humans, who may find it. The files are protected from bitrot, software decay and hardware failure via a transformation from magnetic transitions on a disk to ink on paper, safe for centuries. Instructions in a symbolic language explain the origin of the book and indicate how the content is to be decoded. La Société Anonyme noted that "the package will be encountered and the book decoded only if there will be advanced civilizations on earth in the far future. But the launching of this 'bottle' into the cosmic 'ocean' says something very hopeful about art on this planet." Thus the record is best seen as a time capsule and a statement rather than an attempt to preserve SKOR for future art historians. The SKOR Codex is a project by La Société Anonyme.

Univers Else
-----

![](images/universelse_B.png)

* Design and development: OSP, Speculoos 
* Tools: Fonzie, potrace
* Year: ?
* [http://ospublish.constantvzw.org/foundry/univers-else/](http://ospublish.constantvzw.org/foundry/univers-else/)

> Univers Else is an experiment, a first attempt to escape the post ’80 era of geometrical purity that is so typical of Postscript vector based font drawing. The shapes of Univers Else were obtained from scanning printed textpages that were optically composed by cheap phototypesetting machines in the sixties and seventies. Some of Univers Else beautiful features are: round angles, floating baselines, erratic kerning. 
>
> More precisely in this case, George Maciunas of the Fluxus group used an IBM composer (probably a Selectric typewriter) for most of his own work, and as a former designer, for all Fluxus work. In the 1988 book *Fluxus Codex*, kindly given to Pierre Huyghebaert by Sylvie Eyberg, the body text is typeset in a charmingly rounded and dancing Univers that seems to smile playfully at its dry Swiss creator. As if it was really tempted, trying to provide a beautiful warm up to this old modernist classical. Along with a free, open and libre version! 
>
> A book about contemporary architecture in Wallonia and Brussels provided an ass-kicking opportunity. Three weights were needed. We were not sure that we’ll have the time to finish the fonts in time before having to send the pdf file to the printer. So we laid out the publication with the standard Univers family, and replaced it at the last moment with our version. Suddenly, the 286 pages book jumped out of the ice! 
>
> Different scans were assembled by Grégoire Vigneron following different grids. These huge bitmaps were processed with appropriate potrace settings by the Fonzie software through a .ufo font format as a working format, and an OpenType as output. Some testing and fine-tuning was done by Pierre Marchand, Delphine Platteeuw and Pierre Huyghebaert in FontForge and the font was ready, in a finished state enough to typeset the book. The oblique versions was simply slanted on the fly. 

The Weise7 in/compatible Laboratorium Archive 
----

![](images/weise7-book-home5.jpg)

* Editing: **Kristoffer Gansing**
* Design and development : **Bengt Sjolen**
* Tools: **OpenWRT GNU/Linux**, **InDesign** (?)
* Year: **2013**
* Publisher: **Weise7**

The Weise7 in/compatible Laboratorium Archive is a record of the Weise7 Studio show for Labor Berlin 8, a series of workshops run at Weise7 and its own creation.
Unlike other books, this book acts as an Internet independent wireless server, running from a tiny, custom designed computer inside the book.
To start the server just open the book and connect to it wirelessly using your laptop, tablet or phone. Browse the contents in a park, library or while half way up a mountain, sharing the content with those in your vicinity. Inside can be found high resolution images of our exhibition, video, source code and texts about each of the works. To turn off the server, simply close the book.

The Weise7 in/compatible Laboratorium Archive was launched on March 9th at Labor Berlin at the Haus der Kulturen der Welt, Berlin, and was 5 months in development.
It hosts images, video, source code, schematics and texts covering all aspects of the exhibition, workshop and book production, served to your browser directly from our book. 
The Weise7 in/compatible Laboratorium Archive is served to you directly from a tiny computer, designed especially for this book by studio partner Bengt Sjolen.
The board runs the OpenWRT GNU/Linux distribution, customised for this device. The entire source code for this distribution is available in the archive. Full schematics of the board are also available, making it possible for others to customise and manufacture their own version of this unique device.

ebooks HEAD
----

* Year: **2014**

ebooks ERG
----

* Year: **2014**
