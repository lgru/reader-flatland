Friday July 18
===============

AL initiated repo 

FS tried to find e-pub editing tools: Sigil seems weapon of choice but all li nux packages have been disabled. Compiling from source creates problems (QT). Trying to understand e-pub zips and how to edit them. Calibre maybe? Very awkward.

FS adding e-pub etc. to repo; epub with illustrations simply mimics html (so no ascii)

AL: Looking into media queries; only available for e-pub 3

AL: Ebooks are basically a bunch of HTML/CSS files, how can I edit them with my usual tools? (vim) Looking for a vim plugin to edit zipped files...

AL: typing "vim edit ebooks" in google only returns ebooks about vim

AL: ebooks + git: avoid the compression situation so vim can track the content of the epub

Monday August 12
===============

We try to select a chapter from Flatland to begin with. Re-reading, we are increasingly concerned by the default hierarchies that are in the book: higher dimension is more advanced. This would make it difficult to tell our story.

We look for an alternative text to work from. AL remembers Club Francais du Livre (great covers, but which text to pick?); we try to pick a text from Cory Doctorow because we like his position on e-books ("The distinctive value of ebooks is orthagonal to the value of paper books, and it revolves around the mix-ability and send-ability of electronic text. The more you constrain an ebook's distinctive value propositions — that is, the more you restrict a reader's ability to copy, transport or transform an ebook — the more it has to be valued on the same axes as a paper-book."
http://www.gutenberg.org/cache/epub/11077/pg11077.html), but we can't decide which text would fit (too long, off topic, too loosely written). 

We find a strange essay we like in Gutenberg: Ross Rocklynne: "Sorry, wrong dimension". Not sure if the pop-culture reference works or not, so we decide to test from Flatland and return to this question later.

AL looks at Sigil - seems straightforward as an editor, though not too exciting

We go through a template for e-pub and wonder about ibook specifications; seems all very mysterious





